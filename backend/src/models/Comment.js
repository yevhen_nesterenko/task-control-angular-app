const mongoose = require('mongoose');
const Joi = require('joi');

const { Schema } = mongoose;

const commentJoiSchema = Joi.object({
  text: Joi.string().min(1).max(200).required(),
});

const commentSchema = new Schema({
  boardId: {
    type: mongoose.Types.ObjectId,
    required: true
  },
  taskId: {
    type: mongoose.Types.ObjectId,
    required: true
  },
  text: {
    type: String,
    required: true,
    trim: true
  }
});

const Comment = mongoose.model('Comment', commentSchema);

module.exports = { Comment, commentJoiSchema }
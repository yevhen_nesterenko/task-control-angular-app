const mongoose = require('mongoose');
const Joi = require('joi');

const { Schema } = mongoose;

const taskJoiSchema = Joi.object({
  name: Joi.string().min(1).max(100).required(),
});

const taskSchema = new Schema({
  boardId: {
    type: mongoose.Types.ObjectId,
    required: true
  },
  name: {
    type: String,
    required: true,
    trim: true
  },
  type: {
    type: String,
    required: true,
  },
  createdDate: {
    type: Date,
    required: true,
  },
});

const Task = mongoose.model('Task', taskSchema);

module.exports = { Task, taskJoiSchema }
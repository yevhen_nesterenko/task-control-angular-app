const mongoose = require('mongoose');
const Joi = require('joi');

const { Schema } = mongoose;

const boardJoiSchema = Joi.object({
  name: Joi.string().min(1).max(30).required(),
  description: Joi.string().min(1).max(100).required()
});

const boardSchema = new Schema({
  _userId: {
    type: mongoose.Types.ObjectId,
    required: true
  }, 
  name: {
    type: String,
    required: true,
    trim: true
  },
  description: {
    type: String,
    required: true,
    trim: true
  },
  todo: {
    type: Number,
    required: true,
  },
  inprogress: {
    type: Number,
    required: true,
  },
  done: {
    type: Number,
    required: true,
  },
  columnToDoColor: {
    type: String,
    default: '#9e86e0'
  },
  columnInProgressColor: {
    type: String,
    default: '#9e86e0'
  },
  columnDoneColor: {
    type: String,
    default: '#9e86e0'
  },
  createdDate: {
    type: Date,
    required: true,
  },
});

const Board = mongoose.model('Board', boardSchema);
module.exports = {
  Board,
  boardJoiSchema,
};

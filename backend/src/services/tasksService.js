const { Task } = require('../models/Task');
const { Board } = require('../models/Board');

const saveTask = async (_userId, boardId, name, type) => {
  const board = await Board.findOne({
    _id: boardId,
    _userId
  })
  if(!board) {
    return false
  }
  const task = new Task({
    boardId,
    name,
    type,
    createdDate: new Date(),
  });
  const savedTask = await task.save();
  return savedTask;
};

const findTasks = async (boardId, _userId) => {
  const tasks = await Task.find({
    boardId
  });
  return tasks;
};

const findTasksByType = async (boardId, type) => {
  const tasks = await Task.find({
    boardId, type
  });
  return tasks;
};

const findTask = async (taskId) => {
  const task = await Task.findById(taskId);
  return task;
};

const updateTask = async (_userId, boardId, taskId, payload) => {
  const board = await Board.findOne({
    _id: boardId,
    _userId
  })
  if(!board) {
    return false
  }
  const updatedTask = await Task.findOneAndUpdate({ _id: taskId}, {
    $set: payload
  });
  return updatedTask;
};

const removeTask = async (_userId, boardId, taskId) => {
  const board = await Board.findOne({
    _id: boardId,
    _userId
  })
  if(!board) {
    return false
  }
  const deletedTask = await Task.findByIdAndDelete(taskId);
  return deletedTask;
};

const removeTasks = async (boardId) => {
  const deletedTasks = await Task.deleteMany({boardId});
  return deletedTasks;
};


module.exports = {
  saveTask,
  findTasks,
  findTask,
  updateTask,
  removeTask,
  findTasksByType,
  removeTasks
};


const _ = require('lodash');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');
const bcrypt = require('bcryptjs');
const { User } = require('../models/User');

const saveUser = async (email, password) => {
  const user = new User({
    email,
    password: await bcrypt.hash(password, 10)
  });

  const savedUser = await user.save();
  if(!savedUser) {
    throw new Error();
  }
  const refreshToken = await createSession(user);
  const accessToken = generateAccessAuthToken(user);
  const newUser = {
    _id: savedUser._id,
    email
  }
  return {refreshToken, accessToken, newUser};
};

const findUser = async (email, password) => {

  const user = await findByCredentials(email, password);
  console.log(user)
  if(!user) {
    return user;
  }
  const refreshToken = await createSession(user);
  const accessToken = generateAccessAuthToken(user);

  return {refreshToken, accessToken, user};
}

const findAccessToken = async (user) => {
  const accessAuthToken = generateAccessAuthToken(user);
  return accessAuthToken;
}

// Helpers

const createSession = async (user) => {
  const refreshToken = generateRefreshAuthToken();
  const userWithSession = await saveSessionToDatabase(user, refreshToken)
  if(!userWithSession) {
    throw new Error('Failed to save session to database.')
  }
  return refreshToken;
}

const generateRefreshAuthToken = () => {
  return crypto.randomBytes(64).toString("hex")
}

const saveSessionToDatabase = async (user, refreshToken) => {
  const expiresAt = generateRefreshTokenExpiryTime();
  user.sessions.push({ 'token': refreshToken, expiresAt });
  const userWithSession = await user.save();
  if(!userWithSession) {
    throw new Error();
  }
  return refreshToken
}

const generateRefreshTokenExpiryTime = () => {
  const daysUntilExpire = "10";
  const secondsUntilExpire = ((daysUntilExpire * 24) * 60) * 60;
  return ((Date.now() / 1000) + secondsUntilExpire);
}

const generateAccessAuthToken = (user) => {
  return signJWT(user._id)
}

const signJWT = (id) => {
  const payload = { _id: id.toHexString()};
  const jwtToken = jwt.sign(payload, process.env.JWT_SECRET, { expiresIn: "15m" });
  return jwtToken;
};

const findByCredentials = async (email, password) => {
  const userInDb = await User.findOne({ email });
  if(!userInDb) {
    return null
  }
  const checkPassword = await bcrypt.compare(password, userInDb.password);
  if(!checkPassword) {
    return null
  }
  return userInDb
}

module.exports = {
  saveUser,
  findUser,
  findAccessToken
};
const { Board } = require('../models/Board');

const saveBoard = async (name, description, createdDate, todo, inprogress, done, _userId) => {
  const board = new Board({
    _userId,
    name,
    description,
    createdDate,
    todo,
    inprogress,
    done
  });
  const savedBoard = await board.save();
  return savedBoard;
};

const findBoards = async (_userId) => {
  const boards = await Board.find({
    _userId
  });
  return boards;
};

const findBoard = async (boardId) => {
  const board = await Board.findById(boardId);
  return board;
};

const updateBoard = async (boardId, _userId, payload) => {
  const updatedBoard = await Board.findOneAndUpdate({ _id: boardId, _userId}, {
    $set: payload
  });
  return updatedBoard;
};

const removeBoard = async (boardId, _userId) => {
  const deletedBoard = await Board.findOneAndRemove({_id: boardId, _userId});
  return deletedBoard;
};


module.exports = {
  saveBoard,
  findBoards,
  findBoard,
  updateBoard,
  removeBoard
};
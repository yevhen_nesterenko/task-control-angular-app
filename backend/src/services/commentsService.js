const { Comment } = require('../models/Comment');
const { Board } = require('../models/Board');

const saveComment = async (_userId, boardId, taskId, text) => {
  const board = await Board.findOne({
    _id: boardId,
    _userId
  })
  if(!board) {
    return false
  }
  const comment = new Comment({
    boardId,
    taskId,
    text
  });
  const savedComment = await comment.save();
  return savedComment;
};

const findComments = async (taskId) => {
  const comments = await Comment.find({
    taskId
  });
  return comments;
};

const findComment = async (commentId) => {
  const comment = await Comment.findById(commentId);
  return comment;
};


const updateComment = async (_userId, boardId, commentId, payload) => {
  const board = await Board.findOne({
    _id: boardId,
    _userId
  })
  if(!board) {
    return false
  }
  const updatedComment = await Comment.findOneAndUpdate({ _id: commentId}, {
    $set: payload
  });
  return updatedComment;
};

const removeComment = async (_userId, boardId, commentId) => {
  const board = await Board.findOne({
    _id: boardId,
    _userId
  })
  if(!board) {
    return false
  }
  const deletedComment = await Comment.findByIdAndDelete(commentId);
  return deletedComment;
};

const removeTaskComments = async (taskId) => {
  const deletedComments = await Comment.deleteMany({taskId});
  return deletedComments;
};

const removeBoardComments = async (boardId) => {
  const deletedComments = await Comment.deleteMany({boardId});
  return deletedComments;
};


module.exports = {
  saveComment,
  findComments,
  findComment,
  updateComment,
  removeComment,
  removeTaskComments,
  removeBoardComments
};

const express = require('express');

const router = express.Router();

const { registerUser, loginUser, getAccessToken } = require('../controllers/usersController');
const { verifySessionMiddleware } = require('../middleware/verifySessionMiddleware');

const asyncWrapper = (controller) => (req, res, next) => controller(req, res, next).catch(next);

router.post('/signup', asyncWrapper(registerUser));
router.post('/login', loginUser);
router.get('/me/access-token', verifySessionMiddleware, getAccessToken);

module.exports = {
  usersRouter: router,
};
const express = require('express');

const { authenticate } = require('../middleware/authenticateMiddleware');

const router = express.Router();
const { getBoards, postBoard, getBoard, patchBoard, deleteBoard} = require('../controllers/boardsController');
const { getTasks, postTask, getTask, patchTask, deleteTask } = require('../controllers/tasksController');
const { getComments, postComment, getComment, patchComment, deleteComment } = require('../controllers/commentsController');

const asyncWrapper = (controller) => (req, res, next) => controller(req, res, next).catch(next);

// Boards
router.get('/', authenticate, asyncWrapper(getBoards));
router.post('/', authenticate, asyncWrapper(postBoard));
router.patch('/:boardId', authenticate, asyncWrapper(patchBoard));
router.delete('/:boardId', authenticate, asyncWrapper(deleteBoard));

// Tasks
router.get('/:boardId/tasks', authenticate, asyncWrapper(getTasks));
router.post('/:boardId/tasks', authenticate, asyncWrapper(postTask));
router.patch('/:boardId/tasks/:taskId', authenticate, asyncWrapper(patchTask));
router.delete('/:boardId/tasks/:taskId', authenticate, asyncWrapper(deleteTask));

// Comments
router.get('/:boardId/tasks/:taskId/comments', authenticate, asyncWrapper(getComments));
router.post('/:boardId/tasks/:taskId/comments', authenticate, asyncWrapper(postComment));
router.patch('/:boardId/tasks/:taskId/comments/:commentId', authenticate, asyncWrapper(patchComment));
router.delete('/:boardId/tasks/:taskId/comments/:commentId', authenticate, asyncWrapper(deleteComment));

module.exports = {
  appRouter: router,
};
const { commentJoiSchema } = require('../models/Comment');
const {
  findComments, saveComment, findComment, updateComment, removeComment
} = require('../services/commentsService');

//http://localhost:3000/api/boards/:id/tasks?type=DONE

const getComments = async (req, res) => {
  const {
    taskId
  } = req.params;

  const taskComments = await findComments(taskId);

  if (!taskComments) {
    return res.status(400).json({
      message: 'This task does not have comments',
    });
  }
  return res.send(taskComments)
}

const postComment = async (req, res) => {
  const {
    text
  } = req.body;
  await commentJoiSchema.validateAsync({
    text
  });
  const savedComment = await saveComment(req.user_id, req.params.boardId, req.params.taskId, text);
  if (!savedComment) {
    throw new Error();
  }
  return res.send({ savedComment });
}

const getComment = async (req, res) => {
  const taskComment = await findComment(req.params.commentId);
  if (!taskComment) {
    return res.status(400).json({
      message: 'This task does not have this comment',
    });
  }
  res.send(taskComment)
}

const deleteComment = async (req, res) => {
  const deletedComment = await removeComment(req.user_id, req.params.boardId, req.params.commentId);
  if (!deletedComment) {
    return res.status(400).json({
      message: 'This task does not have this Comment and does not have permission to delete it!',
    });
  }
  res.send({ message: 'Comment deleted successfully!' })
}

module.exports = {
  getComments,
  postComment,
  getComment,
  deleteComment
};
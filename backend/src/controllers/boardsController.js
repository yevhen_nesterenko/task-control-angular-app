const { boardJoiSchema } = require('../models/Board');
const {
  findBoards, saveBoard, findBoard, updateBoard, removeBoard
} = require('../services/boardsService');
const {
  removeTasks
} = require('../services/tasksService');
const {
  removeBoardComments
} = require('../services/commentsService');

const getBoards = async (req, res) => {
  const userBoards = await findBoards(req.user_id);
  if (!userBoards) {
    return res.status(400).json({
      message: 'This user does not have boards',
    });
  }
  res.send(userBoards)
}

const postBoard = async (req, res) => {
  const {
    name, 
    description, 
    createdDate,
  } = req.body;
  await boardJoiSchema.validateAsync({
    name, description
  });
  const savedBoard = await saveBoard(name, description, createdDate, 0, 0, 0, req.user_id);
  if (!savedBoard) {
    throw new Error();
  }
  return res.send(savedBoard);
}

const getBoard = async (req, res) => {
  const userBoard = await findBoard(req.params.boardId);
  if (!userBoard) {
    return res.status(400).json({
      message: 'This user does not have this board',
    });
  }
  res.send(userBoard)
}

const patchBoard = async (req, res) => {
  const updatedBoard = await updateBoard(req.params.boardId, req.user_id, req.body);
  if (!updatedBoard) {
    return res.status(400).json({
      message: 'This user does not have this board and does not have permission to update it!',
    });
  }
  res.send({ message: 'Board updated successfully!' })
}

const deleteBoard = async (req, res) => {
  const deletedBoard = await removeBoard(req.params.boardId, req.user_id);
  const deletedTasks = await removeTasks(req.params.boardId);
  const deletedComments = await removeBoardComments(req.params.boardId);
  if (!deletedBoard || !deletedTasks || !deletedComments) {
    return res.status(400).json({
      message: 'This user does not have this board and does not have permission to delete it!',
    });
  }
  res.send({ message: 'Board deleted successfully!' })
}

module.exports = {
  getBoards,
  postBoard,
  getBoard,
  patchBoard,
  deleteBoard
};
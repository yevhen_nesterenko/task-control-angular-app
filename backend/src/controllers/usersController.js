const { userJoiSchema } = require('../models/User');
const {
  saveUser, findUser, findAccessToken
} = require('../services/usersService');

const registerUser = async (req, res) => {
  const {
    email,
    password
  }= req.body;

  await userJoiSchema.validateAsync({
    email, password
  });

  const usersInfo = await saveUser(email, password);
  if (!usersInfo) {
    throw new Error();
  }
  res
    .header('x-refresh-token', usersInfo.refreshToken)
    .header('x-access-token', usersInfo.accessToken)
    .send(usersInfo.newUser);
}

const loginUser = async (req, res) => {
  const {
    email,
    password
  }= req.body;

  const usersInfo = await findUser(email, password);
  if (!usersInfo) {
    return res.status(401).send({message: 'Wrong password or email!'})
  }
  return res
    .header('x-refresh-token', usersInfo.refreshToken)
    .header('x-access-token', usersInfo.accessToken)
    .send(usersInfo.user);
}

const getAccessToken = async (req, res) => {
  const accessToken = await findAccessToken(req.userObject);
  if(!accessToken) {
    res.status(400).send({"error": "Some error occured!"});
  }

  res.header('x-access-token', accessToken).send({ accessToken });
}

module.exports = {
  registerUser,
  loginUser,
  getAccessToken
};
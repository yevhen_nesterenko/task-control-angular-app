const { taskJoiSchema } = require('../models/Task');
const {
  findTasks, findTasksByType, saveTask, findTask, updateTask, removeTask
} = require('../services/tasksService');
const {
  updateBoard, findBoard
} = require('../services/boardsService');
const {
  removeTaskComments
} = require('../services/commentsService');


//http://localhost:3000/api/boards/:id/tasks?type=DONE

const getTasks = async (req, res) => {
  const {
    boardId
  } = req.params;
  const {
    type
  } = req.query;

  if(!type) {
    const userTasks = await findTasks(boardId);
    if (!userTasks) {
      return res.status(400).json({
        message: 'This board does not have tasks',
      });
    }
    return res.send(userTasks)
  }
  const userTasks = await findTasksByType(boardId, type);
  if (!userTasks) {
    return res.status(400).json({
      message: 'This board does not have tasks',
    });
  }
  return res.send(userTasks)  

}

const postTask = async (req, res) => {
  const {
    name, type
  } = req.body;
  await taskJoiSchema.validateAsync({
    name
  });
  const neededBoard = await findBoard(req.params.boardId);
  const newAmountOfTasksType = neededBoard[`${type}`] + 1;
  const updatedBoard = await updateBoard(req.params.boardId, req.user_id, {[`${type}`]: newAmountOfTasksType});
  console.log(updatedBoard)
  const savedTask = await saveTask(req.user_id, req.params.boardId, name, type);
  if (!savedTask || !updatedBoard) {
    throw new Error();
  }
  return res.send(savedTask);
}

const getTask = async (req, res) => {
  const userTask = await findTask(req.params.taskId);
  if (!userTask) {
    return res.status(400).json({
      message: 'This user does not have this Task',
    });
  }
  res.send(userTask)
}

const patchTask = async (req, res) => {
  if(req.body.type) {
    const currTask = await findTask(req.params.taskId);
    const oldType = currTask.type;
    const neededBoard = await findBoard(req.params.boardId);
    const newAmountOfNewTasksType = neededBoard[`${req.body.type}`] + 1;
    const newAmountOfOldTasksType = neededBoard[`${oldType}`] - 1;
    const updatedBoard = await updateBoard(req.params.boardId, req.user_id, {[`${oldType}`]: newAmountOfOldTasksType, [`${req.body.type}`]: newAmountOfNewTasksType});
    if (!updatedBoard) {
      throw new Error();
    }
  }
  const updatedTask = await updateTask(req.user_id, req.params.boardId, req.params.taskId, req.body);
  if (!updatedTask) {
    return res.status(400).json({
      message: 'This user does not have this Task and does not have permission to update it!',
    });
  }
  res.send({ updatedTask })
}

const deleteTask = async (req, res) => {
  const neededBoard = await findBoard(req.params.boardId);
  const currTask = await findTask(req.params.taskId);
  const newAmountOfTasksType = neededBoard[`${currTask.type}`] - 1;
  const updatedBoard = await updateBoard(req.params.boardId, req.user_id, {[`${currTask.type}`]: newAmountOfTasksType});
  const deletedTask = await removeTask(req.user_id, req.params.boardId, req.params.taskId);
  const deletedComments = await removeTaskComments(req.params.taskId);
  if (!updatedBoard || !deletedTask || !deletedComments) {
    return res.status(400).json({
      message: 'This user does not have this Task and does not have permission to delete it!',
    });
  }
  res.send({ message: 'Task deleted successfully!' })
}

module.exports = {
  getTasks,
  postTask,
  getTask,
  patchTask,
  deleteTask
};
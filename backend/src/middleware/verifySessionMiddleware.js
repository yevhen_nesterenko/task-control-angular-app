const { User } = require('../models/User');

let verifySessionMiddleware = async (req, res, next) => {
  const refreshToken = req.header('x-refresh-token');
  const _id = req.header('_id');

  try {
    const userInDB = await findByIdAndToken(_id, refreshToken);
    if(!userInDB) {
      return res.send({
        'error': 'User not found. Make sure that the refresh token and user id are correct'
      })
    }

    req.user_id = userInDB._id;
    req.userObject = userInDB;
    req.refreshToken = refreshToken;
    let isSessionValid = false;

    userInDB.sessions.forEach((session) => {
      if (session.token === refreshToken) {
        const isRefreshTokenExpired = hasRefreshTokenExpired(session.expiresAt);
        if (isRefreshTokenExpired === false) {
          isSessionValid = true;
        }
      }
    });

    if (isSessionValid) {
      next();
    } else {
      return res.send({
        'error': 'Refresh token has expired or the session is invalid'
      })
    }
  } 
  catch(err) {
    res.status(401).send(err);
  }
}

const hasRefreshTokenExpired = (expiresAt) => {
  let secondsSinceEpoch = Date.now() / 1000;
  if (expiresAt > secondsSinceEpoch) {
    return false;
  } else {
    return true;
  }
}

const findByIdAndToken = async (_id, token) => {
  return User.findOne({
    _id,
    'sessions.token': token
  });
}


module.exports = {
  verifySessionMiddleware,
};
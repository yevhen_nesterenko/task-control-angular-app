const jwt = require('jsonwebtoken');

require('dotenv').config();

const authenticate = (req, res, next) => {
  let token = req.header('x-access-token');
  if (!token) {
    return res.status(401).json({ message: 'Please, include access token to request!' });
  }
  try {
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    req.user_id = decoded._id;
    return next();
  } catch (err) {
    return res.status(401).json({ message: err.message });
  }
}

module.exports = {
  authenticate
};
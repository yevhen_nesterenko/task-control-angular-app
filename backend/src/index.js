const express = require('express');
const morgan = require('morgan');

const app = express();
const mongoose = require('mongoose');
const { appRouter } = require('./routers/appRouter');
const { usersRouter } = require('./routers/usersRouter');

require('dotenv').config();

app.use(express.json());
app.use(morgan('tiny'));

// CORS HEADERS MIDDLEWARE
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, POST, HEAD, OPTIONS, PUT, PATCH, DELETE");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-access-token, x-refresh-token, _id");

  res.header(
      'Access-Control-Expose-Headers',
      'x-access-token, x-refresh-token'
  );

  next();
});

app.use('/api/users', usersRouter);
app.use('/api/boards', appRouter);


mongoose.connect(process.env.DB_CONN).then(() => {
  app.listen(process.env.PORT, () => {
    console.log(`🚀 Server ready at ${process.env.PORT}`);
  });
})
.catch((err) => {
  console.error(`Error on server startup: ${err.message}`);
  process.exit(1);
});


// ERROR HANDLER

function errorHandler(err, req, res, next) {
  res.status(500).send({ message: err.message });
  next();
}

app.use(errorHandler);

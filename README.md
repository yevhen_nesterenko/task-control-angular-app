# ANGULAR TASK CONTROL APP
This is an application that allows you to plan your plans, breaking them down into small tasks into categories "To Do", "In Prgoress", "Done" and drag tasks from one category to another . You can also add comments to youe task and change color of columns with tasks and many many other features! 
The entire application divided in two parts: frontend and backend.

`frontend` folder consist angular app.

`backend` folder consist nodejs server.

## Install
  Open `frontend` folder in your console and run command:
    `npm i`

## Run the app
  Open `frontend` folder in your console and run command ( to run server and frontend at the same time ):
    `npm run serve`

## Debug frontend
  Open `frontend` folder in your console and run command:
    `ng serve`

## Debug backend
  Open `backend` folder in your console and run command:
    `npm run debug`

## Additional software

- Angular
- Node.js
- MongoDB
- Postman (not necessary)

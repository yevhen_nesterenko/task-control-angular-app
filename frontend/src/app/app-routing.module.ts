// import { NgModule } from '@angular/core';
// import { RouterModule, Routes } from '@angular/router';
// import { AuthComponent } from './pages/auth/auth.component';
// import { LoginComponent } from './pages/auth/login/login.component';
// import { SignupComponent } from './pages/auth/signup/signup.component';
// import { BoardComponent } from './pages/board/board.component';
// import { CommentsAddComponent } from './pages/board/comments-add/comments-add.component';
// import { TasksEditComponent } from './pages/board/tasks-edit/tasks-edit.component';
// import { TasksInfoComponent } from './pages/board/tasks-info/tasks-info.component';
// import { BoardsEditComponent } from './pages/dashboard/boards-edit/boards-edit.component';
// import { DashboardResolverService } from './pages/dashboard/dashboard-resolver.service';
// import { DashboardComponent } from './pages/dashboard/dashboard.component';

// const routes: Routes = [
//   {
//     path: '',
//     redirectTo: '/auth/login',
//     pathMatch: 'full',
//   },
//   {
//     path: 'dashboard',
//     component: DashboardComponent,
//     children: [
//       {path: 'new', component: BoardsEditComponent},
//       {path: ':id/edit', component: BoardsEditComponent, resolve: [DashboardResolverService]}
//     ]
//   },
//   {
//     path: 'board/:boardId',
//     component: BoardComponent,
//     children: [
//       {path: ':columnName/new', component: TasksEditComponent},
//       {path: ':columnName/:taskId/edit', component: TasksEditComponent},
//       {path: ':columnName/:taskId/comments', component: CommentsAddComponent},
//       {path: ':columnName/:taskId/info', component: TasksInfoComponent}
//     ]
//   },
//   {
//     path: 'auth', component: AuthComponent,
//     children: [
//       {path: 'login', component: LoginComponent},
//       {path: 'signup', component: SignupComponent}
//     ]
//   }
// ];

// @NgModule({
//   imports: [RouterModule.forRoot(routes)],
//   exports: [RouterModule],
// })
// export class AppRoutingModule {}

import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

const appRoutes: Routes = [
  { path: '', redirectTo: '/recipes', pathMatch: 'full'},
  {
    path: 'auth',
    loadChildren: () => import('./pages/auth/auth.module').then(m => m.AuthModule),
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./pages/dashboard/dashboard.module').then(m => m.DashboardModule),
  },
  {
    path: 'board',
    loadChildren: () => import('./pages/board/board.module').then(m => m.BoardModule),
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}

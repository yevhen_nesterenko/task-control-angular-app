import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { Store } from '@ngrx/store';
import { Actions, ofType } from '@ngrx/effects';
import { take, map, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

import { Board } from '../../models/board.model';
import * as fromApp from '../../store/app.reducer';
import * as DashboardActions from '../dashboard/store/dashboard.actions';

@Injectable({ providedIn: 'root' })
export class DashboardResolverService implements Resolve<{boards: Board[]}> {
  constructor(
    private store: Store<fromApp.AppState>,
    private actions$: Actions
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    return this.store.select('boards').pipe(
      take(1),
      map(dashboardState => {
        return dashboardState.boards;
      }),
      switchMap(boards => {
        if (boards.length === 0) {
          this.store.dispatch(DashboardActions.getBoardsAPI());
          return this.actions$.pipe(
            ofType(DashboardActions.setBoards),
            take(1)
          );
        } else {
          return of({boards});
        }
      })
    );
  }
}

import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import * as fromApp from '../../store/app.reducer';
import * as DashboardActions from './store/dashboard.actions';

@Component({
  selector: 'app-dashboards',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  searchFilterInput = '';
  sortingDirectionInput = 'asc';
  sortingTypeInput = 'sort-date';

  constructor(private store: Store<fromApp.AppState>) { }

  ngOnInit(): void {
    this.store.dispatch(DashboardActions.getBoardsAPI());
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BoardsEditComponent } from './boards-edit/boards-edit.component';
import { DashboardComponent } from './dashboard.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      { path: 'new', component: BoardsEditComponent },
      { path: ':id/edit', component: BoardsEditComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule {}

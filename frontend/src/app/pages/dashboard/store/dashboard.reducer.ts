import { Board } from "src/app/models/board.model";
import { Action, createReducer, on } from '@ngrx/store';
import * as DashboardActions from '../store/dashboard.actions';

export interface State {
  boards: Board[];
}

const initialState: State = {
  boards: []
};

export const _dashboardReducer = createReducer(
  initialState,
  on(
    DashboardActions.addBoard,
    (state, action) => ({
      ...state,
      boards: state.boards.concat({ ...action.board })
    })
  ),
  on(
    DashboardActions.updateBoard,
    (state, action) => ({
      ...state,
      boards: state.boards.map(
        (board, index) => index === action.index ? { ...action.board } : board
      )
    })
  ),
  on(
    DashboardActions.deleteBoard,
    (state, action) => ({
      ...state,
      boards: state.boards.filter(
        (_, index) => index !== action.index
      )
    })
  ),
  on(
    DashboardActions.setBoards,
    (state, action) => ({
      ...state,
      boards: [ ...action.boards ]
    })
  )
)

export function dashboardReducer(state: State, action: Action) {
  return _dashboardReducer(state, action);
}

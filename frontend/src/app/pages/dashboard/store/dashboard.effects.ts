import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { switchMap, map, withLatestFrom } from 'rxjs/operators';

import * as DashboardActions from './dashboard.actions';
import * as fromApp from '../../../store/app.reducer';
import { DashboardService } from '../dashboard.service';


@Injectable()
export class DashboardEffects {

  fetchBoards$ = createEffect(() =>
    this.actions$.pipe(
      ofType(DashboardActions.getBoardsAPI),
      withLatestFrom(this.store.select('boards')),
      switchMap(() => this.dashboardService.getBoard()),
      map(boards => {
        return boards.map(board => {
          return {
            ...board
          };
        });
      }),
      map(boards => {
        return DashboardActions.setBoards({boards});
      })
    )
  );

  saveNewBoard$ = createEffect(() => {
    return this.actions$
      .pipe(
        ofType(DashboardActions.saveBoardAPI),
        switchMap((action) => {
          return this.dashboardService.createBoard(action.board).pipe(
            map((board) => {
              const boards = [...Object.values(action.boards).flat()];
              const boardStoreIndex = boards.indexOf(boards.find(el => JSON.stringify(el) === JSON.stringify(action.board)));
              boards[boardStoreIndex] = board;
              return DashboardActions.setBoards({boards});
            })
          );
        })
      );
  })

  updateBoard$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DashboardActions.updateBoardAPI),
      switchMap((action) => {
        return this.dashboardService.updateBoard(action.board, action.board._id).pipe(
          map((data) => {
            return DashboardActions.updateBoardAPISuccess({ response: data });
          })
        );
      })
    );
  });

  deleteBoard$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DashboardActions.deleteBoardAPI),
      switchMap((action) => {
        return this.dashboardService.deleteBoard(action.id).pipe(
          map((data) => {
            return DashboardActions.deleteBoardAPISuccess({ id: action.id });
          })
        );
      })
    );
  });

  constructor(
    private actions$: Actions,
    private store: Store<fromApp.AppState>,
    private dashboardService: DashboardService
  ) {}
}

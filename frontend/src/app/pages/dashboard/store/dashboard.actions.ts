import { createAction, props } from '@ngrx/store';
import { Board } from 'src/app/models/board.model';

export const addBoard = createAction(
  '[Dashboard] Add Board',
  props<{
    board: Board
  }>()
)

export const updateBoard = createAction(
  '[Dashboard] Update Board',
  props<{
    index: number,
    board: Board
  }>()
);

export const deleteBoard= createAction(
  '[Dashboard] Delete Board',
  props<{
    index: number
  }>()
);

export const setBoards = createAction(
  '[Dashboard] Set Boards',
  props<{
    boards: Board[]
  }>()
);


export const getBoardsAPI = createAction(
  '[Dashboard] Get Boards API'
);


export const saveBoardAPI = createAction(
  '[Dashboard] Save Board API',
  props<{
    board: Board
    boards: Board[]
  }>()
);

export const saveBoardAPISuccess = createAction(
  '[Dashboard] Save Board API Success',
  props<{
    response: Board
  }>()
);

export const updateBoardAPI = createAction(
  '[Dashboard] Update Board API',
  props<{
    board: Board
  }>()
);

export const updateBoardAPISuccess = createAction(
  '[Dashboard] Update Board API Success',
  props<{
    response: Board
  }>()
);

export const deleteBoardAPI = createAction(
  '[Dashboard] Delete Board API',
  props<{ id: string }>()
)

export const deleteBoardAPISuccess = createAction(
  '[Dashboard] Delete Board API Success',
  props<{ id: string }>()
)

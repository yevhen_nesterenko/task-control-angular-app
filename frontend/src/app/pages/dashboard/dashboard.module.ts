import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { BoardsListComponent } from './boards-list/boards-list.component';
import { BoardsItemComponent } from './boards-list/boards-item/boards-item.component';
import { BoardsEditComponent } from './boards-edit/boards-edit.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { StoreModule } from '@ngrx/store';

import * as fromDashboard from './store/dashboard.reducer';



@NgModule({
  declarations: [
    DashboardComponent,
    BoardsListComponent,
    BoardsItemComponent,
    BoardsEditComponent
  ],
  imports: [
    ReactiveFormsModule,
    RouterModule,
    DashboardRoutingModule,
    SharedModule,
    StoreModule.forFeature('boards', fromDashboard.dashboardReducer),
    FormsModule
  ]
})
export class DashboardModule { }

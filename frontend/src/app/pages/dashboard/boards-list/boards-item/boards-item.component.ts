import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Board } from 'src/app/models/board.model';
import * as fromApp from '../../../../store/app.reducer';
import * as DashboardActions from '../../store/dashboard.actions';

@Component({
  selector: 'app-boards-item',
  templateUrl: './boards-item.component.html',
  styleUrls: ['./boards-item.component.scss']
})
export class BoardsItemComponent implements OnInit {
  @Input() board: Board;
  @Input() index: number;
  @Input() filterWord: string;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private store: Store<fromApp.AppState>) { }

  ngOnInit(): void {
    
  }

  onDeleteBoard() {
    this.store.dispatch(DashboardActions.deleteBoard({ index: this.index }));
    this.store.dispatch(DashboardActions.deleteBoardAPI({ id: this.board._id }));
  }

  onShowBoardInfo() {
    this.router.navigate(['/board', this.board._id], { relativeTo: this.route });
  }
}

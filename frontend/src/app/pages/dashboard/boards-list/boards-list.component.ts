import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Board } from 'src/app/models/board.model';
import { Store } from '@ngrx/store';

import * as fromApp from '../../../store/app.reducer';
import { map, Subscription } from 'rxjs';

@Component({
  selector: 'app-boards-list',
  templateUrl: './boards-list.component.html',
  styleUrls: ['./boards-list.component.scss']
})
export class BoardsListComponent implements OnInit {

  boards: Board[];
  boardsInStore: Board[];
  subscription: Subscription;
  @Input() searchText: string;
  @Input() sortingDirection: string;
  @Input() sortingType: string;

  constructor(private router: Router, private route: ActivatedRoute, private store: Store<fromApp.AppState>) { }

  ngOnInit(): void {
    this.subscription = this.store
      .select('boards')
      .pipe(map(boardsState => boardsState.boards))
      .subscribe((boards: Board[]) => {
        this.boardsInStore = boards
        this.boards = [...boards];
      });
  }
}

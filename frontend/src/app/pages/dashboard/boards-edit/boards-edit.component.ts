import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { map, Subscription, take } from 'rxjs';
import { Board } from 'src/app/models/board.model';
import * as fromApp from '../../../store/app.reducer';
import * as DashboardActions from '../store/dashboard.actions';

@Component({
  selector: 'app-boards-edit',
  templateUrl: './boards-edit.component.html',
  styleUrls: ['./boards-edit.component.scss']
})

export class BoardsEditComponent implements OnInit, OnDestroy {
  id: number;
  editMode = false;
  dashboardForm: FormGroup;
  savedBoard: Board;

  private storeSub: Subscription;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private store: Store<fromApp.AppState>) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.id = +params['id'];
      this.editMode = params['id'] != null;
      this.initForm();
    });
  }

  onSubmit() {
    if (this.editMode) {
      let board: Board = {...this.savedBoard, ...this.dashboardForm.value};
      this.store.dispatch(DashboardActions.updateBoard({index: this.id, board: board}));
      this.store.dispatch(DashboardActions.updateBoardAPI({board}));
    } else {
      let board: Board = {...this.dashboardForm.value, createdDate: new Date()};
      this.store.dispatch(DashboardActions.addBoard({board}));
      const boards: any = this.getState(this.store)['boards'];
      this.store.dispatch(DashboardActions.saveBoardAPI({board, boards}));
    }
    this.onCancel()
  }

  onCancel() {
    this.router.navigate(['/dashboard'], { relativeTo: this.route });
  }

  ngOnDestroy(): void {
    if (this.storeSub) {
      this.storeSub.unsubscribe();
    }
  }

  private initForm() {
    let boardName = '';
    let boardDescription = '';
    let oldBoard: Board;

    if (this.editMode) {
      this.storeSub = this.store
        .select('boards')
        .pipe(
          map(dashboardState => {
            return dashboardState.boards.find((board, index) => {
              return index === this.id;
            });
          })
        )
        .subscribe((board: Board) => {
          oldBoard = {...board}
          boardName = board.name;
        });
      this.savedBoard = {...oldBoard};
      this.dashboardForm = new FormGroup({
        name: new FormControl(boardName, Validators.required)
      });
    } else {
      this.dashboardForm = new FormGroup({
        name: new FormControl(boardName, Validators.required),
        description: new FormControl(boardDescription, Validators.required),
      });
    }
  }

  private getState(store: Store<fromApp.AppState>): fromApp.AppState {
    let state: fromApp.AppState;
    store.pipe(take(1)).subscribe(s => state = s);
    return state;
  }
}

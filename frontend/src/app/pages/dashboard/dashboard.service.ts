import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Board } from '../../models/board.model';

@Injectable({
  providedIn: 'root',
})

export class DashboardService {
  constructor(private http: HttpClient) {}

  getBoard() {
    return this.http.get<Board[]>('http://localhost:3000/api/boards');
  }

  createBoard(payload: Board) {
    return this.http.post<Board>('http://localhost:3000/api/boards', payload);
  }

  updateBoard(payload: Board, id: string) {
    return this.http.patch<Board>(
      `http://localhost:3000/api/boards/${id}`,
      payload
    );
  }

  deleteBoard(id: string) {
    return this.http.delete(`http://localhost:3000/api/boards/${id}`);
  }
}

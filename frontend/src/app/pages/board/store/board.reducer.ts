import { Action, createReducer, on } from '@ngrx/store';
import * as BoardActions from '../store/board.actions';
import { Task } from "src/app/models/task.model";

export interface State {
  tasks: Task[];
}

const initialState: State = {
  tasks: []
};


export const _boardReducer = createReducer(
  initialState,
  on(
    BoardActions.addTask,
    (state, action) => ({
      ...state,
      tasks: state.tasks.concat({ ...action.task })
    })
  ),
  on(
    BoardActions.updateTask,
    (state, action) => ({
      ...state,
      tasks: state.tasks.map(
        (task, index) => index === action.index ? { ...action.task } : task
      )
    })
  ),
  on(
    BoardActions.deleteTask,
    (state, action) => ({
      ...state,
      tasks: state.tasks.filter(
        (_, index) => index !== action.index
      )
    })
  ),
  on(
    BoardActions.setTasks,
    (state, action) => ({
      ...state,
      tasks: [ ...action.tasks ]
    })
  )
)

export function boardReducer(state: State, action: Action) {
  return _boardReducer(state, action);
}

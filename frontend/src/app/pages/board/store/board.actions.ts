import { createAction, props } from '@ngrx/store';
import { Task } from 'src/app/models/task.model';

export const addTask = createAction(
  '[Board] Add Task',
  props<{
    task: Task
  }>()
)

export const updateTask = createAction(
  '[Board] Update Task',
  props<{
    index: number,
    task: Task
  }>()
);

export const deleteTask = createAction(
  '[Board] Delete Task',
  props<{
    index: number
  }>()
);

export const setTasks = createAction(
  '[Board] Set Tasks',
  props<{
    tasks: Task[]
  }>()
);

export const getTasksAPI = createAction(
  '[Board] Get Tasks API',
  props<{ boardId: string }>()
);

export const saveTaskAPI = createAction(
  '[Dashboard] Save Task API',
  props<{
    boardId: string,
    task: Task,
    tasks: Task[]
  }>()
);

export const updateTaskAPI = createAction(
  '[Board] Update Task API',
  props<{
    task: Task
  }>()
);

export const updateTaskAPISuccess = createAction(
  '[Board] Update Task API Success',
  props<{
    response: Task
  }>()
);

export const deleteTaskAPI = createAction(
  '[Board] Delete Task API',
  props<{ task: Task }>()
)

export const deleteTaskAPISuccess = createAction(
  '[Dashboard] Delete Board API Success',
  props<{ response: Object }>()
)

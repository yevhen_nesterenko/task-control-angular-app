import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { switchMap, map, withLatestFrom } from 'rxjs/operators';

import * as BoardActions from './board.actions';
import * as fromApp from '../../../store/app.reducer';
import { BoardService } from '../board.service';


@Injectable()
export class BoardEffects {

  fetchTasks$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BoardActions.getTasksAPI),
      switchMap((action) => this.boardService.getTask(action.boardId)),
      map(tasks => {
        return tasks.map(task => {
          return {
            ...task
          };
        });
      }),
      map(tasks => {
        return BoardActions.setTasks({tasks});
      })
    )
  );

  saveNewTask$ = createEffect(() => {
    return this.actions$
      .pipe(
        ofType(BoardActions.saveTaskAPI),
        switchMap((action) => {
          return this.boardService.createTask(action.boardId, action.task).pipe(
            map((task) => {
              const tasks = [...Object.values(action.tasks).flat()];
              const taskStoreIndex = tasks.indexOf(tasks.find(el => JSON.stringify(el) === JSON.stringify(action.task)));
              tasks[taskStoreIndex] = task;
              return BoardActions.setTasks({tasks});
            })
          );
        })
      );
  })

  updateTask$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(BoardActions.updateTaskAPI),
      switchMap((action) => {
        return this.boardService.updateTask(action.task).pipe(
          map((data) => {
            return BoardActions.updateTaskAPISuccess({ response: data });
          })
        );
      })
    );
  });

  deleteTask$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(BoardActions.deleteTaskAPI),
      switchMap((action) => {
        return this.boardService.deleteTask(action.task).pipe(
          map((data) => {
            return BoardActions.deleteTaskAPISuccess({ response: data });
          })
        );
      })
    );
  });

  constructor(
    private actions$: Actions,
    private store: Store<fromApp.AppState>,
    private boardService: BoardService
  ) {}
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BoardComponent } from './board.component';
import { CommentsAddComponent } from './comments-add/comments-add.component';
import { TasksEditComponent } from './tasks-edit/tasks-edit.component';
import { TasksInfoComponent } from './tasks-info/tasks-info.component';

const routes: Routes = [
  {
    path: ':boardId',
    component: BoardComponent,
    children: [
      { path: ':columnName/new', component: TasksEditComponent },
      { path: ':columnName/:taskId/edit', component: TasksEditComponent },
      { path: ':columnName/:taskId/comments', component: CommentsAddComponent },
      { path: ':columnName/:taskId/info', component: TasksInfoComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BoardRoutingModule {}

import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Task } from 'src/app/models/task.model';
import { map, Subscription, take } from 'rxjs';
import * as fromApp from '../../../store/app.reducer';
import { Store } from '@ngrx/store';
import * as BoardActions from '../store/board.actions';
import { BoardService } from '../board.service';

@Component({
  selector: 'app-comments-add',
  templateUrl: './comments-add.component.html',
  styleUrls: ['./comments-add.component.scss']
})
export class CommentsAddComponent implements OnInit {
  taskId: string;
  commentId: string;
  commentsForm: FormGroup;
  boardId: string;

  constructor(private router: Router, private route: ActivatedRoute, private boardService: BoardService) { }

  ngOnInit(): void {
    this.boardId = this.route.parent.params['_value'].boardId;
    this.taskId = this.route.params['_value'].taskId
    this.initForm();
  }

  onCancel() {
    this.router.navigate(['../../../'], { relativeTo: this.route });
  }

  onSubmit() {
    this.boardService.createComment(this.boardId, this.taskId, this.commentsForm.value).subscribe();
    this.onCancel()
  }

  private initForm() {
    let text = '';
    this.commentsForm = new FormGroup({
      text: new FormControl(text, Validators.required),
    });
  }
}

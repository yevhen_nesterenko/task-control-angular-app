import { Component, OnInit, Input } from '@angular/core';
import { Task } from 'src/app/models/task.model';
import * as fromApp from '../../../../store/app.reducer';
import { Store } from '@ngrx/store';
import * as BoardActions from '../../store/board.actions';

@Component({
  selector: 'app-tasks-item',
  templateUrl: './tasks-item.component.html',
  styleUrls: ['./tasks-item.component.scss']
})
export class TasksItemComponent implements OnInit {
  @Input() task: Task;
  @Input() index: number;
  @Input() filterWord: string;

  constructor(private store: Store<fromApp.AppState>) {

  }

  ngOnInit(): void {
  }

  onDeleteBoard() {
    this.store.dispatch(BoardActions.deleteTask({ index: this.index }));
    this.store.dispatch(BoardActions.deleteTaskAPI({ task: this.task }));
  }
}

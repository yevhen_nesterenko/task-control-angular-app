import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Component, Input, OnInit } from '@angular/core';
import { Task } from 'src/app/models/task.model';
import * as fromApp from '../../../store/app.reducer';
import { Store } from '@ngrx/store';
import * as BoardActions from '../store/board.actions';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-tasks-list',
  templateUrl: './tasks-list.component.html',
  styleUrls: ['./tasks-list.component.scss']
})
export class TasksListComponent implements OnInit {
  @Input() columnId: string;
  @Input() tasksList: Task[];
  @Input() tasksInStore: Task[];
  @Input() searchTextInput: string;
  @Input() sortingDirectionInput: string;
  @Input() sortingTypeInput: string;

  constructor(private store: Store<fromApp.AppState>, private route: ActivatedRoute) { }

  ngOnInit(): void {
  }

  drop(event: CdkDragDrop<string[]>, type: string) {
    // this.store.dispatch(BoardActions.getTasksAPI({boardId: this.route.params['_value'].boardId}));
    const task: any = event.previousContainer.data[event.previousIndex];
    const updatedTask = {...task, type};
    const boardId = this.route.params['_value'].boardId;
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      this.store.dispatch(BoardActions.updateTask({index: this.tasksInStore.indexOf(task), task: updatedTask}));
      this.store.dispatch(BoardActions.updateTaskAPI({ task: updatedTask}));
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }
  }

}

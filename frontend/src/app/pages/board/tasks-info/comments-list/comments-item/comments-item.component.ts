import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Comment } from 'src/app/models/comment.model';
import { BoardService } from '../../../board.service';

@Component({
  selector: 'app-comments-item',
  templateUrl: './comments-item.component.html',
  styleUrls: ['./comments-item.component.scss']
})
export class CommentsItemComponent implements OnInit {
  @Input() comment: Comment;
  @Output() removeTask = new EventEmitter<Comment>();
  constructor() { }

  ngOnInit(): void {
  }

  onRemoveTask() {
    this.removeTask.emit(this.comment);
  }
}

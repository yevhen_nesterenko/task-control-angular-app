import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Comment } from 'src/app/models/comment.model';
import { BoardService } from '../../board.service';

@Component({
  selector: 'app-comments-list',
  templateUrl: './comments-list.component.html',
  styleUrls: ['./comments-list.component.scss']
})
export class CommentsListComponent implements OnInit {
  comments: Comment[];
  boardId: string;
  taskId: string;
  constructor(private route: ActivatedRoute, private boardService: BoardService) { }

  ngOnInit(): void {
    this.boardId = this.route.parent.params['_value'].boardId;
    this.taskId = this.route.params['_value'].taskId;
    this.boardService.getComments(this.boardId, this.taskId).subscribe((responseData: Comment[]) => {
      this.comments = responseData;
    })
  }

  onRemoveTask(comment: Comment) {
    this.comments = this.comments.filter(el => {
      return JSON.stringify(el) !== JSON.stringify(comment)
    });
    this.boardService.deleteComment(this.boardId, this.taskId, comment._id).subscribe();
  }
}

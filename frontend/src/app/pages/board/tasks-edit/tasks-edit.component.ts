import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Task } from 'src/app/models/task.model';
import { map, Subscription, take } from 'rxjs';
import * as fromApp from '../../../store/app.reducer';
import { Store } from '@ngrx/store';
import * as BoardActions from '../store/board.actions';

@Component({
  selector: 'app-tasks-edit',
  templateUrl: './tasks-edit.component.html',
  styleUrls: ['./tasks-edit.component.scss']
})

export class TasksEditComponent implements OnInit {
  taskId: number;
  columnName: string;
  editMode = false;
  tasksForm: FormGroup;
  savedTask: Task;
  boardId: string;

  private storeSub: Subscription;

  constructor(private router: Router, private route: ActivatedRoute, private store: Store<fromApp.AppState>) { }

  ngOnInit(): void {
    this.boardId = this.route.parent.params['_value'].boardId;
    this.route.params.subscribe((params: Params) => {
      this.columnName = params['columnName'];
      this.taskId = +params['taskId'];
      this.editMode = params['taskId'] != null;
      this.initForm();
    });
  }

  onCancel() {
    if (this.editMode) {
      this.router.navigate(['../../../'], { relativeTo: this.route });
    } else {
      this.router.navigate(['../../'], { relativeTo: this.route });
    }

  }

  onSubmit() {
    if (this.editMode) {
      let task: Task = {...this.savedTask, ...this.tasksForm.value};
      this.store.dispatch(BoardActions.updateTask({index: this.taskId, task}));
      this.store.dispatch(BoardActions.updateTaskAPI({task}));
    } else {
      let task: Task = {...this.tasksForm.value, createdDate: new Date(), type: this.columnName};
      this.store.dispatch(BoardActions.addTask({task}));
      const tasks: any = this.getState(this.store)['tasks'];
      this.store.dispatch(BoardActions.saveTaskAPI({boardId: this.boardId, task, tasks}))
    }
    this.onCancel()
  }

  private initForm() {
    let taskName = '';
    let oldTask: Task;
    if (this.editMode) {
      this.storeSub = this.store
        .select('tasks')
        .pipe(
          map(boardState => {
            return boardState.tasks.find((task, index) => {
              return index === this.taskId;
            });
          })
        )
        .subscribe((task: Task) => {
          oldTask = {...task}
          taskName = task.name;
        });
      this.savedTask = {...oldTask};
      this.tasksForm = new FormGroup({
        name: new FormControl(taskName, Validators.required)
      });
    } else {
      this.tasksForm = new FormGroup({
        name: new FormControl(taskName, Validators.required),
      });
    }
  }

  private getState(store: Store<fromApp.AppState>): fromApp.AppState {
    let state: fromApp.AppState;
    store.pipe(take(1)).subscribe(s => state = s);
    return state;
  }
}

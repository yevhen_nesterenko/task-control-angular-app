import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Column } from 'src/app/models/column.model';
import { map, Subscription, take } from 'rxjs';
import { Task } from 'src/app/models/task.model';
import { Store } from '@ngrx/store';
import * as fromApp from '../../../store/app.reducer';
import * as DashboardActions from '../../dashboard/store/dashboard.actions';

@Component({
  selector: 'app-column-list',
  templateUrl: './column-list.component.html',
  styleUrls: ['./column-list.component.scss']
})
export class ColumnListComponent implements OnInit {
  columns: Column[] = [
      {
        id: 'todo',
        name: 'To Do',
        tasks: [],
        color: ''
      },
      {
        id: 'inprogress',
        name: 'In Progress',
        tasks: [],
        color: ''
      },
      {
        id: 'done',
        name: 'Done',
        tasks: [],
        color: ''
      }
  ];
  tasks: Task[];
  subscription: Subscription;
  board: any;
  idInStore: any;
  @Input() searchText: string;
  @Input() sortingDirection: string;
  @Input() sortingType: string;
  //   {
  //     id: 'todo',
  //     name: 'To Do',
  //     tasks: [
  //       'Lorem ipsum',
  //       'foo',
  //       "This was in the 'Research' columnnnnnnnnnn This was in the 'Research' columnnnnnnnnnn",
  //     ],
  //     color: '#9e86e0'
  //   },
  //   {
  //     id: 'inprogress',
  //     name: 'In Progress',
  //     tasks: [
  //       'Get to work',
  //       'Pick up groceries',
  //       'Go home',
  //       'Fall asleep',
  //     ],
  //     color: '#9e86e0'
  //   },
  //   {
  //     id: 'done',
  //     name: 'Done',
  //     tasks: [
  //       'Get up',
  //       'Brush teeth',
  //       'Take a shower',
  //       'Check e-mail',
  //       'Walk dog',
  //     ],
  //     color: '#9e86e0'
  //   }
  // ];

  constructor(private route: ActivatedRoute, private router: Router, private store: Store<fromApp.AppState>) {}

  ngOnInit() {
    this.board = { ...Object.values(this.getState(this.store)['boards']).flat().find(el => el._id === this.route.params['_value'].boardId)};
    this.idInStore = Object.values(this.getState(this.store)['boards']).flat().map(el => JSON.stringify(el)).indexOf(JSON.stringify(this.board));

    if(this.board) {
      this.columns[0].color = this.board.columnToDoColor;
      this.columns[1].color = this.board.columnInProgressColor;
      this.columns[2].color = this.board.columnDoneColor;
    }
    this.subscription = this.store
      .select('tasks')
      .pipe(map(tasksState => tasksState.tasks))
      .subscribe((tasks: Task[]) => {
        this.tasks = [...tasks];
        this.columns.forEach(column => this.updateColumn(column.id, tasks))
      });

  }

  onChangeColor(columnId: string) {
    switch(columnId){
      case 'todo':
        this.board.columnToDoColor = this.columns[0].color
        break;
      case 'inprogress':
        this.board.columnInProgressColor = this.columns[1].color
        break;
      case 'done':
        this.board.columnDoneColor = this.columns[2].color
        break;
    }
    this.store.dispatch(DashboardActions.updateBoard({index: this.idInStore, board: this.board}));
    this.store.dispatch(DashboardActions.updateBoardAPI({board:this.board}));
  }

  onAddNewTask(columnId: string) {
    this.router.navigate([columnId, 'new'], { relativeTo: this.route });
  }

  private updateColumn(id: string, tasks: Task[]) {
    const neededTasks = tasks.filter((task: Task) => task.type === id );
    this.columns.find(column => column.id === id).tasks = neededTasks;
  }

  private getState(store: Store<fromApp.AppState>): fromApp.AppState {
    let state: fromApp.AppState;
    store.pipe(take(1)).subscribe(s => state = s);
    return state;
  }
}

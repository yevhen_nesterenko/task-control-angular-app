import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Task } from 'src/app/models/task.model';
import { Comment } from 'src/app/models/comment.model';

@Injectable({
  providedIn: 'root',
})

export class BoardService {
  constructor(private http: HttpClient) {}

  getTask(boardId: string) {
    return this.http.get<Task[]>(`http://localhost:3000/api/boards/${boardId}/tasks`);
  }

  createTask(boardId: string, payload: Task) {
    return this.http.post<Task>(`http://localhost:3000/api/boards/${boardId}/tasks`, payload);
  }

  updateTask( payload: Task) {
    return this.http.patch<Task>(
      `http://localhost:3000/api/boards/${payload.boardId}/tasks/${payload._id}`,
      payload
    );
  }

  deleteTask(payload: Task) {
    return this.http.delete(`http://localhost:3000/api/boards/${payload.boardId}/tasks/${payload._id}`);
  }

  getComments(boardId: string, taskId: string) {
    return this.http.get<Comment[]>(`http://localhost:3000/api/boards/${boardId}/tasks/${taskId}/comments`);
  }

  createComment(boardId: string, taskId: string, payload: Task) {
    return this.http.post<Comment>(`http://localhost:3000/api/boards/${boardId}/tasks/${taskId}/comments`, payload);
  }

  deleteComment(boardId: string, taskId: string, commnetId: string) {
    return this.http.delete(`http://localhost:3000/api/boards/${boardId}/tasks/${taskId}/comments/${commnetId}`);
  }
}

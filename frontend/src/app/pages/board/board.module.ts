import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BoardComponent } from './board.component';
import { ColumnListComponent } from './column-list/column-list.component';
import { CommentsAddComponent } from './comments-add/comments-add.component';
import { TasksEditComponent } from './tasks-edit/tasks-edit.component';
import { TasksInfoComponent } from './tasks-info/tasks-info.component';
import { CommentsListComponent } from './tasks-info/comments-list/comments-list.component';
import { CommentsItemComponent } from './tasks-info/comments-list/comments-item/comments-item.component';
import { TasksListComponent } from './tasks-list/tasks-list.component';
import { TasksItemComponent } from './tasks-list/tasks-item/tasks-item.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { BoardRoutingModule } from './board-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { DragDropModule } from '@angular/cdk/drag-drop';
import * as fromBoard from './store/board.reducer';
import { StoreModule } from '@ngrx/store';


@NgModule({
  declarations: [
    BoardComponent,
    ColumnListComponent,
    CommentsAddComponent,
    TasksEditComponent,
    TasksInfoComponent,
    CommentsListComponent,
    CommentsItemComponent,
    TasksListComponent,
    TasksItemComponent
  ],
  imports: [
    ReactiveFormsModule,
    RouterModule,
    BoardRoutingModule,
    SharedModule,
    DragDropModule,
    StoreModule.forFeature('tasks', fromBoard.boardReducer),
    CommonModule,
    FormsModule
  ]
})
export class BoardModule { }

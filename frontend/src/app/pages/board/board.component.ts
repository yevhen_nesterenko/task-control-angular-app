import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { map, Subscription } from 'rxjs';
import { Board } from 'src/app/models/board.model';

import * as fromApp from '../../store/app.reducer';
import * as BoardActions from './store/board.actions';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss'],
})

export class BoardComponent implements OnInit {
  board: Board
  subscription: Subscription;
  searchFilterInput = '';
  sortingDirectionInput = 'asc';
  sortingTypeInput = 'sort-date';
  constructor(private route: ActivatedRoute, private store: Store<fromApp.AppState>, private router: Router) {}

  ngOnInit() {
    this.store.dispatch(BoardActions.getTasksAPI({boardId: this.route.params['_value'].boardId}));
    this.subscription = this.store
      .select('boards')
      .pipe(map(boardsState => boardsState.boards))
      .subscribe((boards: Board[]) => {
        if(boards.length === 0) {
          this.router.navigate(['/dashboard'], { relativeTo: this.route });
        }
        this.board = boards.find(el => el._id === this.route.params['_value'].boardId)
      });
  }
}

import {createAction, props} from '@ngrx/store';
import { User } from 'src/app/models/user.model';

export const loginStart = createAction(
  '[Auth] Login Start',
  props<{
    email: string;
    password: string
  }>()
);

export const signupStart = createAction(
  '[Auth] Signup Start',
  props<{
    email: string;
    password: string
  }>()
);


export const authenticateSuccess = createAction(
  '[Auth] Authenticate Success',
  props<{
    _id: string;
    email: string
  }>()
);


export const authenticateFail = createAction(
  '[Auth] Authenticate Fail',
  props<{
    errorMessage: string
  }>()
);

export const logout = createAction(
  '[Auth] Logout'
);

import { Action, createReducer, on } from '@ngrx/store';
import { User } from '../../../models/user.model';
import * as AuthActions from './auth.actions';


export interface State {
  user: User;
  authError: string;
}


const initialState: State = {
  user: null,
  authError: null
};


export const _authReducer = createReducer(
  initialState,
  on(
    AuthActions.loginStart,
    AuthActions.signupStart,
    (state) => ({
      ...state,
      authError: null
    })
  ),

  on(
    AuthActions.authenticateSuccess,
    (state, action) => ({
      ...state,
      authError: null,
      loading: false,
      user: new User(
        action._id,
        action.email
      )
    })
  ),

  on(
    AuthActions.authenticateFail,
    (state, action) => ({
      ...state,
      user: null,
      authError: action.errorMessage,
    })
  ),

  on(
    AuthActions.logout,
    (state) => ({
      ...state,
      user: null
    })
  ),

);


export function authReducer(state: State, action: Action) {
  return _authReducer(state, action);
}

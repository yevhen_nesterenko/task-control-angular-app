import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { switchMap, catchError, map, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import * as AuthActions from './auth.actions';
import { AuthService } from '../auth.service';


const handleError = (errorRes: any) => {
  let errorMessage = 'An unknown error occurred!';

  if (!errorRes.error) {
    return of(AuthActions.authenticateFail({errorMessage}));
  }

  const errArr = errorRes.error.message.split(' ');

  if(errArr.indexOf('E11000') !== -1) {
    errorMessage = 'This email exists already!';
  }

  if(errArr.indexOf('password') !== -1 || errArr.indexOf('email') !== -1) {
    errorMessage = 'This email does not exist or this password is not correct!';
  }
  return of(AuthActions.authenticateFail({errorMessage}));
};


@Injectable()
export class AuthEffects {

  authLogin$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.loginStart),
      switchMap(action => {
        return this.authService.login(action.email, action.password)
          .pipe(
            map((resData: any) => {
              const {_id, email} = resData.body;
              this.router.navigate(['/dashboard']);
              return AuthActions.authenticateSuccess({_id, email});
            }),
            catchError(errorRes => {
              return handleError(errorRes);
            })
          );
      })
    )
  );

  authSignup$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.signupStart),
      switchMap(action => {
        return this.authService.signup(action.email, action.password)
          .pipe(
            map((resData: any) => {
              const {_id, email} = resData.body;
              this.router.navigate(['/dashboard']);
              return AuthActions.authenticateSuccess({_id, email});
            }),
            catchError(errorRes => {
              return handleError(errorRes);
            })
          );
      })
    )
  );

  authLogout$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.logout),
      tap(() => {
        this.authService.logout();
      })
    ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private http: HttpClient,
    private router: Router,
    private authService: AuthService
  ) {}


}

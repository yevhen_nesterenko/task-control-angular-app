import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { shareReplay, tap } from 'rxjs';

interface AuthTokenData {
  _id: string;
  expiresAt: string;
  token: string;
}

interface AuthResponseData {
  _id: string;
  email: string;
  password: string;
  sessions: AuthTokenData[];
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient, private router: Router,) { }

  login(email: string, password: string) {
    return this.http.post<AuthResponseData>(`http://localhost:3000/api/users/login`, {email, password}, {observe: 'response'}).pipe(
      shareReplay(),
      tap((res: HttpResponse<any>) => {
        this.setSession(res.body._id, res.headers.get('x-access-token'),  res.headers.get('x-refresh-token'))
    }))
  }

  logout() {
    this.removeSession();
    this.router.navigate(['/auth/login']);
  }

  signup(email: string, password: string) {
    return this.http.post<AuthResponseData>(`http://localhost:3000/api/users/signup`, {email, password}, {observe: 'response'}).pipe(
      shareReplay(),
      tap((res: HttpResponse<any>) => {
        this.setSession(res.body._id, res.headers.get('x-access-token'),  res.headers.get('x-refresh-token'))
    }))
  }

  private setSession(userId: string, accessToken: string, refreshToken: string) {
    localStorage.setItem('user-id', userId);
    localStorage.setItem('x-access-token', accessToken);
    localStorage.setItem('x-refresh-token', refreshToken);
  }

  private removeSession() {
    localStorage.removeItem('user-id');
    localStorage.removeItem('x-access-token');
    localStorage.removeItem('x-refresh-token');
  }

  getAccessToken() {
    return localStorage.getItem('x-access-token');
  }

  getRefreshToken() {
    return localStorage.getItem('x-refresh-token');
  }

  getUserId() {
    return localStorage.getItem('user-id');
  }

  setAccessToken(accessToken: string) {
    localStorage.setItem('x-access-token', accessToken);
  }

  getNewAccessToken() {
    return this.http.get('http://localhost:3000/api/users/me/access-token', {
      headers: {
        'x-refresh-token': this.getRefreshToken(),
        '_id': this.getUserId()
      },
      observe: 'response'
    }).pipe(
      tap((res: HttpResponse<any>) => {
        this.setAccessToken(res.headers.get('x-access-token'))
      })
    )
  }


}

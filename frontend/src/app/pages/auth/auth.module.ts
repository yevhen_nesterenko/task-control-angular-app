import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthComponent } from './auth.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AuthRoutingModule } from './auth-routing.module';
import * as fromAuth from './store/auth.reducer';
import { StoreModule } from '@ngrx/store';



@NgModule({
  declarations: [AuthComponent, LoginComponent, SignupComponent],
  imports: [
    RouterModule,
    FormsModule,
    CommonModule,
    AuthRoutingModule,
    StoreModule.forFeature('auth', fromAuth.authReducer)
  ]
})
export class AuthModule { }

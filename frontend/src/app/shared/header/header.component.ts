import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { take } from 'rxjs';
import * as AuthActions from '../../pages/auth/store/auth.actions';
import * as fromApp from '../../store/app.reducer';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  email: string;

  constructor(private store: Store<fromApp.AppState>, private router: Router) { }

  ngOnInit(): void {
    this.email = this.getState(this.store)['auth'].user?.email;
    if(!this.email) {
      this.router.navigate(['/auth/login'])
    }
  }

  onLogout() {
    this.store.dispatch(AuthActions.logout())
  }

  private getState(store: Store<fromApp.AppState>): fromApp.AppState {
    let state: fromApp.AppState;
    store.pipe(take(1)).subscribe(s => state = s);
    return state;
  }
}

import { Pipe, PipeTransform } from '@angular/core';
import { Board } from 'src/app/models/board.model';

@Pipe({
  name: 'sort',
  pure: false
})
export class SortPipe implements PipeTransform {

  transform(value: any, sortingDirection: string, sortingType: string): any {
    switch(sortingType) {
      case 'sort-date':
        if(sortingDirection === 'asc') {
          return value.sort((a: Board, b: Board) => {
            if((new Date(a.createdDate)) > (new Date(b.createdDate))) {
              return 1
            } else {
              return -1
            }
          })
        } else {
          return value.sort((a: Board, b: Board) => {
            if((new Date(a.createdDate)) < (new Date(b.createdDate))) {
              return 1
            } else {
              return -1
            }
          })
        }
      case 'sort-name':
        if(sortingDirection === 'asc') {
          return value.sort((a: Board, b: Board) => a.name.localeCompare(b.name))
        } else {
          return value.sort((a: Board, b: Board) => b.name.localeCompare(a.name))
        }
      case 'sort-do-tasks':
        if(sortingDirection === 'asc') {
          return value.sort((a: Board, b: Board) => a.todo - b.todo)
        } else {
          return value.sort((a: Board, b: Board) => b.todo - a.todo)
        }
      case 'sort-progress-tasks':
        if(sortingDirection === 'asc') {
          return value.sort((a: Board, b: Board) => a.inprogress - b.inprogress)
        } else {
          return value.sort((a: Board, b: Board) => b.inprogress - a.inprogress)
        }
      case 'sort-done-tasks':
        if(sortingDirection === 'asc') {
          return value.sort((a: Board, b: Board) => a.done - b.done)
        } else {
          return value.sort((a: Board, b: Board) => b.done - a.done)
        }
    }


    return value
  }

}

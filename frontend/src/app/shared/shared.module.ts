import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HighlightDirective } from './custom-directives/highlight.directive';
import { FilterPipe } from './custom-pipes/filter.pipe';
import { SortPipe } from './custom-pipes/sort.pipe';
import { HeaderComponent } from './header/header.component';



@NgModule({
  declarations: [
    HighlightDirective,
    FilterPipe,
    SortPipe,
    HeaderComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    HighlightDirective,
    FilterPipe,
    SortPipe,
    HeaderComponent,
    CommonModule
  ]
})
export class SharedModule {}

export class Task {
  constructor(public boardId: string, public name: string, public type: string, public createdDate: string, public _id: string) {}
}

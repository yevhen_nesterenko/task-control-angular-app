export class Board {
  constructor(public name: string, public description: string, public createdDate: string, public _id: string, public todo: number, public inprogress: number, public done: number) {}
}

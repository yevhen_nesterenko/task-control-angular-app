export class Comment {
  constructor(public taskId: string, public text: string, public _id: string) {}
}

import { Task } from "./task.model";

export class Column {
  constructor(public id: string, public name: string, public tasks: Task[], public color: string) {}
}

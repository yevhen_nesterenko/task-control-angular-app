import { ActionReducerMap } from '@ngrx/store';
import * as fromDashboard from '../pages/dashboard/store/dashboard.reducer';
import * as fromBoard from '../pages/board/store/board.reducer';
import * as fromAuth from '../pages/auth/store/auth.reducer';

export interface AppState {
  boards: fromDashboard.State;
  tasks: fromBoard.State;
  auth: fromAuth.State;
}

export const appReducer: ActionReducerMap<AppState> = {
  boards: fromDashboard.dashboardReducer,
  tasks: fromBoard.boardReducer,
  auth: fromAuth.authReducer
};

// export const appReducer: ActionReducerMap<AppState> = {
//   boards: fromDashboard._dashboardReducer,
//   tasks: fromBoard._boardReducer,
//   auth: fromAuth._authReducer
// };
